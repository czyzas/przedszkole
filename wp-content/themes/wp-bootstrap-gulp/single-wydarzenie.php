<?php get_header(); ?>

<main class="subpage">
	<div class="top">
		<?php include(locate_template('modules/top.php')); ?>
	</div>
    <div class="modules">
        <div class="container">
            <?php include(locate_template('modules/modules-includer.php')); ?>
            <?php include(locate_template('modules/pages.php')); ?>
        </div>
    </div>
</main>

<?php get_footer(); ?>