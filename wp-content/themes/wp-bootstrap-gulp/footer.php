<section class="privacy-policy">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
                <a href="<?php the_field('privacy_policy_link', 'option') ?>">
                    <?php the_field('privacy_policy_text', 'option') ?>
                </a>
            </div>
        </div>
    </div>
</section>
<footer id="kontakt">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-5 offset-md-1">
                <?php the_field('footer_contact', 'option') ?>
            </div>
            <div class="col-6">
                <img src="<?php echo get_field('footer_image', 'option')['url'] ?>" alt="<?php echo get_field('footer_image', 'option')['title'] ?>">
            </div>
        </div>
        <div class="row margin-top">
            <div class="col-12 col-md-5 offset-md-1">
                <?php the_field('footer_info_left', 'option') ?>
            </div>
            <div class="col-12 col-md-6">
                <?php the_field('footer_info_right', 'option') ?>
            </div>
        </div>
        <?php 
            $map = get_field('footer_map', 'option');
            $id = uniqid();
        ?>
        <div class="row margin-top">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="map" id="map-<?php echo $id ?>">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2454.6952159431758!2d21.027651315789313!3d52.030649879724464!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47192f7443972667%3A0x4833932732075a13!2sPrzedszkole+Publiczne+Zegar+S%C5%82oneczny!5e0!3m2!1sen!2spl!4v1542890556222" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>
        </div>

    </div>
</footer>

<?php wp_footer(); ?>

</body>

</html>