<?php

@ini_set( 'upload_max_size' , '64M' );
@ini_set( 'post_max_size', '64M');
@ini_set( 'max_execution_time', '300' );

// IMAGE SIZES
add_image_size( 'fhd', 1920, 0 );

//NAVWALKER MENU
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
register_nav_menus( array(
	'primary' => __( 'Primary Menu', 'wp-bootstrap-gulp' ),
) );

// // REGISTER SIDEBAR
// if ( function_exists('register_sidebar') )
// register_sidebar(array(
// 	'before_widget' => '',
// 	'after_widget' => '',
// 	'before_title' => '<h3>',
// 	'after_title' => '</h3>',
// 	'id' => 'unique_id'
// ));

// REMOVE WP EMOJI
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

// REMOVE WP EMBED
function my_deregister_scripts(){
	wp_deregister_script( 'wp-embed' );
}
add_action( 'wp_footer', 'my_deregister_scripts' );

// REMOVE RECENT COMMENTS
function remove_recent_comments_style() {
	global $wp_widget_factory;
	remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
}
add_action('widgets_init', 'remove_recent_comments_style');

// SVG ENABLE
function cc_mime_types($mimes)
{
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// FIX SVG THUMBNAIL DISPLAY
function fix_svg_thumb_display()
{
	echo '<style>
    td.media-icon img[src$=".svg"], img[src$=".svg"].attachment-post-thumbnail { 
      width: 100% !important; 
      height: auto !important; 
    }
  </style>';
}
add_action('admin_head', 'fix_svg_thumb_display');

//ACF OPTIONS
function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCRPjv9FXcWodJmJ1YU4Nq87F0VMjFl_NE');
}
add_action('acf/init', 'my_acf_init');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page();
}

?>