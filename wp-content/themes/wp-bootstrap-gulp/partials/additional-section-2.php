<?php if ( get_field( 'enable_additional_2' ) ): ?>
    <section class="additional-section" id="dodatkowa-sekcja-2">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h2><?php the_field( 'additional_title_2' ) ?></h2>
                    <ul class="nav nav-tabs" id="tab-additional_2" role="tablist">
                        <?php if ( have_rows( 'additional_tabs_2' ) ) : $i = 1; ?>
                            <?php while ( have_rows( 'additional_tabs_2' ) ) : the_row(); ?>
                                <li class="nav-item">
                                    <a class="nav-link<?php echo $i == 1 ? ' active' : '' ?>" id="tab-additional_2-<?php echo $i ?>" data-toggle="tab" href="#additional_2-<?php echo $i ?>" role="tab" aria-controls="additional_2" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                </li>

                                <?php $i ++; endwhile; ?>

                        <?php endif; ?>
                    </ul>
                </div>
                <div class="col-12 col-md-10 offset-md-1">
                    <div class="tab-content" id="additional-content">
                        <?php if ( have_rows( 'additional_tabs_2' ) ) : $i = 1; ?>
                            <?php while ( have_rows( 'additional_tabs_2' ) ) : the_row(); ?>
                                <div class="tab-pane fade<?php echo $i == 1 ? ' show active' : '' ?>" id="additional_2-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-additional_2-<?php echo $i ?>">
                                    <div class="content-wrapper">
                                        <div class="content">
                                            <?php the_sub_field( 'content' ); ?>
                                        </div>
                                    </div>
                                    <a class="read-more d-none" href="#"><?php the_field( 'additional_read_more_2' ) ?><?php echo file_get_contents( get_template_directory_uri() . '/dist/images/arrow-right.svg' ) ?></a>
                                </div>
                                <?php $i ++; endwhile; ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>