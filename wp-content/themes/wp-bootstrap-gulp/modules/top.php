<div class="row no-gutters">
    <div class="col-12 col-md-10 offset-md-1">
        <h1><?php the_title() ?></h1>
    </div>
	<?php
	if ( false ) {
		$top_image = get_field( 'top_image' );
		if ( ! empty( $top_image ) ) {
			$url = $top_image['url'];
		} else {
			$url = get_template_directory_uri() . '/dist/images/top-image.png';
		}
		?>
        <!--    <div class="top-image col-12 col-md-10 offset-md-1" style="background-image:url('<?php //echo $url;
		?>')"></div>-->
	<?php } ?>
</div>
<?php

$next   = get_next_post();
$prev   = get_previous_post();
$prevID = $prev->ID;
$nextID = $next->ID;
?>
<div class="swiper-navigation">
    <a href="<?php echo get_permalink( $prevID ); ?>" title="<?php echo get_the_title( $prevID ); ?>">
		<?php if ( ! empty( $prev ) ): ?>
            <div class="swiper-button-prev">
				<?php echo file_get_contents( get_template_directory() . '/dist/images/swiper-arrow-left.svg' ) ?>
            </div>
		<?php endif; ?>
    </a>
    <a href="<?php echo get_permalink( $nextID ); ?>" title="<?php echo get_the_title( $nextID ); ?>">
		<?php if ( ! empty( $next ) ): ?>
            <div class="swiper-button-next">
				<?php echo file_get_contents( get_template_directory() . '/dist/images/swiper-arrow-right.svg' ) ?>
            </div>
		<?php endif; ?>
    </a>
</div>