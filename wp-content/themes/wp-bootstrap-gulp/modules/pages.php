<?php

$page_type = get_post_type();

$next = get_next_post();
$prev = get_previous_post();
    

$prev_naming = 'Poprzednie';
$next_naming = 'Więcej';


if($page_type === 'projekt'): 
    $prev = false;
    $next = get_post(26);
    $next_naming = 'powrót do innych projektów';
endif; 



$prevID = $prev->ID;
$nextID = $next->ID;

?>

    <?php if(!empty($prev) || !empty($next)):?>
        <div class="row bottom-padding pages">
            <div class="col-12 col-md-10 offset-md-1">
                <?php if (!empty($prev)): ?>
                <div class="prev-page">
                    <a href="<?php echo get_permalink($prevID); ?>" title="<?php echo get_the_title($prevID); ?>">
                        
                        <div class="arrow left">
                            <?php echo file_get_contents(get_template_directory_uri().'/dist/images/arrow-right.svg')?> 
                        </div>

                        <div><span class="prev-next-title"><?php echo $prev_naming ?></span></div>

                    </a>
                </div>
                <?php endif; ?>
        
                <?php if (!empty($next)) : ?>
                <div class="next-page">

                    <a href="<?php echo get_permalink($nextID); ?>" title="<?php echo get_the_title($nextID); ?>">
            
                        <div><span class="prev-next-title"><?php echo $next_naming ?></span></div>
                        
                        <div class="arrow right">
                            <?php echo file_get_contents(get_template_directory_uri().'/dist/images/arrow-right.svg')?>
                        </div>

                    </a>
                    
                </div>
                <?php endif;?>
            </div>
        </div>
    <?php endif; ?>

