<?php
    if( have_rows('modules') ):
        while ( have_rows('modules') ) : the_row();

            if( get_row_layout() == 'break' ):
                include(locate_template('modules/break.php'));
                
            elseif( get_row_layout() == 'headline' ):
                include(locate_template('modules/headline.php'));    

            elseif( get_row_layout() == 'single_column' ):
                include(locate_template('modules/single-column.php'));

            elseif( get_row_layout() == 'two_columns' ):
                include(locate_template('modules/two-columns.php'));

            endif;  

        endwhile;
    endif;
?>
