<div class="row bottom-padding two-columns">
    <div class="col-12 col-md-5 offset-md-1"><?php the_sub_field('left_column') ?></div>
    <div class="col-12 col-md-5 "><?php the_sub_field('right_column') ?></div>
</div>