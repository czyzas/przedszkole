'use strict';

var gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano'),
    uglify = require('gulp-uglify'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer');

var config = {
    nodeModulesDir: './node_modules',
    srcDir: './src',
    distDir: './dist'
};

function swallowError (error) {
    console.log(error.toString());
    this.emit('end');
}

gulp.task('styles-app', function () {
    return gulp.src(config.srcDir + '/scss/main.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed', errLogToConsole: true}))
        .on('error', swallowError)
        .pipe(autoprefixer({
            browsers: ['last 3 versions']
        }))
        .pipe(sourcemaps.write())
        .pipe(rename('app.min.css'))
        .pipe(gulp.dest(config.distDir + '/css'))
});

gulp.task('styles-dependencies', function () {
    return gulp.src([
            // list of css dependencies
            config.nodeModulesDir + '/bootstrap/dist/css/bootstrap.min.css',
            config.nodeModulesDir + '/@fortawesome/fontawesome-free/css/all.css',
            config.nodeModulesDir + '/swiper/dist/css/swiper.min.css',
        ])
        .pipe(cssnano())
        .pipe(concat('dependencies.min.css'))
        .pipe(gulp.dest(config.distDir + '/css'))
});

gulp.task('js-app', function () {
    return gulp.src([
            config.srcDir + '/js/main.js',
            config.srcDir + '/js/homepage.js',
        ])
        .pipe(sourcemaps.init())
        .pipe(concat('app.min.js'))
        .pipe(uglify())
        .on('error', swallowError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(config.distDir + '/js'))
});

gulp.task('js-dependencies', function () {
    return gulp.src([
            // list of js dependencies
            config.nodeModulesDir + '/jquery/dist/jquery.min.js',
            config.nodeModulesDir + '/popper.js/dist/umd/popper.min.js',
            config.nodeModulesDir + '/bootstrap/dist/js/bootstrap.min.js',
            config.nodeModulesDir + '/swiper/dist/js/swiper.min.js',
            config.nodeModulesDir + '/jquery-parallax.js/parallax.min.js',
        ])
        .pipe(uglify())
        .pipe(concat('dependencies.min.js'))
        .pipe(gulp.dest(config.distDir + '/js'))
});

gulp.task('fonts', function() {
    return gulp.src(config.srcDir + '/webfonts/**/*')
      .pipe(gulp.dest(config.distDir + '/webfonts'))
});

gulp.task('fonts-fa', function() {
    return gulp.src(config.nodeModulesDir + '/@fortawesome/fontawesome-free/webfonts/**/*')
      .pipe(gulp.dest(config.distDir + '/webfonts'))
});

gulp.task('images', function() {
    return gulp.src(config.srcDir + '/images/**/*')
      .pipe(gulp.dest(config.distDir + '/images'))
});

gulp.task('default', [
        'styles-app',
        'styles-dependencies',
        'js-app',
        'js-dependencies',
        'fonts',
        'fonts-fa',
        'images'
    ], function() {
        gulp.watch( config.srcDir + '/scss/*.scss', [ 'styles-app' ] );
        gulp.watch( config.srcDir + '/js/*.js', [ 'js-app' ] );
        gulp.watch( 'webfonts/**/*', { cwd: config.srcDir }, [ 'fonts' ] );
        gulp.watch( 'images/**/*', {cwd: config.srcDir }, [ 'images' ] );
    });