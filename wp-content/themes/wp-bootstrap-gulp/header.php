<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php wp_title( '' ); ?></title>
    <meta name="description" content="<?php bloginfo( 'description' ); ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- styles -->
    <link href="<?php echo get_template_directory_uri() ?>/dist/css/dependencies.min.css" rel="stylesheet"/>
    <link href="<?php echo get_template_directory_uri() ?>/dist/css/app.min.css" rel="stylesheet"/>

    <!-- scripts -->
    <script src="<?php echo get_template_directory_uri() ?>/dist/js/dependencies.min.js" rel="stylesheet"></script>
    <script src="<?php echo get_template_directory_uri() ?>/dist/js/app.min.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCRPjv9FXcWodJmJ1YU4Nq87F0VMjFl_NE"
            type="text/javascript"></script>

    <!-- fonts -->
    <link href="https://fonts.googleapis.com/css?family=PT+Serif|Work+Sans:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<?php wp_head(); ?>

</head>

<body <?php body_class() ?>>
<header>
    <div class="logo">
        <a href="/">
            <span class="blue">zegar</span>
            <span class="orange">słoneczny</span>
        </a>
    </div>
    <div class="right-col">
		<?php
		wp_nav_menu( array(
			'theme_location'  => 'primary',
			'depth'           => 2,
			'container'       => 'nav',
			'container_class' => 'primary-menu-container',
			'container_id'    => '',
			'menu_class'      => 'primary-menu',
			'menu_id'         => 'primary-menu',
			'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
			'walker'          => new WP_Bootstrap_Navwalker(),
		) );
		?>
        <a href="https://www.facebook.com/Przedszkole-Zegar-S%C5%82oneczny-1354700614669900/?epa=SEARCH_BOX" target="_blank" rel="nofollow noopener noreferrer" class="facebook">
			<?php echo file_get_contents( get_template_directory_uri() . '/dist/images/facebook.svg' ) ?></a>
        <div class="hamburger">
            <span class="bar"></span>
            <span class="bar"></span>
            <span class="bar"></span>
        </div>
    </div>
</header>
