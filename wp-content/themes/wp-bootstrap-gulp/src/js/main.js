(function () {
    'use strict';
    $(document).ready(function () {
        //SMOOTH SCROLL WHEN CLICK ON A HREF=#

        $('header .smooth-scroll a').click(function (e) {
            // e.preventDefault();
            if(location.pathname === '/') {
                //homepage
                var target = this.hash;
                var $target = $(target);
                if($target.offset() === undefined) {
                    return;
                }
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top
                }, 900, 'swing');
    
            } else {
                document.location.href="/" + this.hash;
            }
        });

        if(window.location.hash !== '' && location.pathname === '/')  {
            if (location.hash) {
                setTimeout(function() {
              
                  window.scrollTo(0, 0);
                  $('html, body').stop().animate({
                      'scrollTop': $(window.location.hash).offset().top
                  }, 1100, 'swing');
                }, 1);
              }
        }
        
        menuAction();
        $(window).on('resize', menuAction);

        $(window).on('scroll', function () {
            var scrollTop = $(document).scrollTop();
            var offsetTop;
            if($('.front-page').length)
                offsetTop = scrollTop - $('#header-heading').offset().top + $('header').height();
            else
                offsetTop = 1;
            if (offsetTop > 0) {
                $('header').addClass('scrolled');
            } else {
                $('header').removeClass('scrolled');
            }
        });
        
        function menuAction() {
            if($(window).width() < 768) {
                $('.hamburger').off().click(function(){
                    $(this).toggleClass('open');
                    $('nav').slideToggle().toggleClass('collapsed');
                });

                $('.menu-item-has-children>a').off().click(function(e) {
                    e.preventDefault();
                   $(this)
                       .parent()
                       .toggleClass('dropdown-collapse')
                       .children('ul').slideToggle()
                });
                $('.menu-item-has-children ul a').off().click(function(){
                    $('.dropdown-collapse').removeClass('dropdown-collapse').children('ul').slideUp();
                    $('.hamburger').toggleClass('open');
                    $('nav').slideToggle().toggleClass('collapsed');
                });
            } else {
                $('nav').show();
                $('.menu-item-has-children>a').off().click(function (e) {
                    e.preventDefault();
                })
            }
        }


    });
}(jQuery));