(function () {
    'use strict';
    $(document).ready(function () {
        //program tabs
        $('.program .nav-tabs a').on('show.bs.tab', function (e) {
            var $target = $($(e.target).attr('href'));
            if (e.relatedTarget === undefined) $target.slideDown();
            else
                $($(e.relatedTarget).attr('href')).slideUp(
                    function () {
                        $target.slideDown();
                    }
                );

        });
        var programSwiper = new Swiper('.program-swiper', {
            slidesPerView: 5,
            loop: true,
            autoplay: {
                delay: 3000,
                disableOnInteraction: false
            },
            breakpoints: {
                1023: {
                    slidesPerView: 4
                },
                767: {
                    slidesPerView: 3
                },
                575: {
                    slidesPerView: 2
                },
                350: {
                    slidesPerView: 1.5
                }
            },
            navigation: {
                prevEl: $('.program .swiper-button-prev'),
                nextEl: $('.program .swiper-button-next')
            }
        });

        //read-more
        $('.tab-pane.with-read-more').each(function () {
            var animating = false;

            var $this = $(this);

            var containerHeight = $this.find('>.content-wrapper').height();
            var contentHeight = $this.find('>.content-wrapper>.content').height();

            // console.log('containerHeight',containerHeight);
            // console.log('contentHeight',contentHeight);

            if(contentHeight > containerHeight) {
                $this.find('a.read-more').removeClass('d-none');
            }

            $this.find('a.read-more').click(function (e) {

                if(!animating) {
                    animating = true;
                    e.preventDefault();
                    $this.find('>.content-wrapper').stop().animate({
                        height: contentHeight
                    }, 1000, function () {
                       $this.find('a.read-more').fadeOut(function () {
                            animating = false;
                        });
                    });
                }
            });
        });



    });
}(jQuery));