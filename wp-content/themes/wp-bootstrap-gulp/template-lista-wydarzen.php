<?php
/*
	Template name: Lista wydarzeń
*/

get_header(); ?>

<main class="lista-projektow">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10 offset-md-1">
            <h2 class="title">Wydarzenia</h2>
                <div class="row">
                    <?php
                    global $paged;
                    global $wp_query;
                    $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
                $project = new WP_Query( array(
                        'post_type'      => array( 'wydarzenie' ),
                        'posts_per_page' => 4,
                        'meta_query'     => array(),
                        'order'          => 'DESC',
                        'orderby'        => 'date',
                        'paged' => $paged
                    ) );
                    

                    if ($project->have_posts()) :
                        while ($project->have_posts()) :
                            $project->the_post();
	                        $top_image = get_field('top_image');
	                        if(!empty($top_image))
		                        $url = $top_image['url'];
	                        else
		                        $url = get_template_directory_uri().'/dist/images/top-image.png';
                            ?>

                            <div class="col-12 col-md-6 project">
                                <a href="<?php the_permalink() ?>">
                                    <div class="img-container">
                                        <img src="<?php echo $url ?>" alt="">
                                        <div class="content">
                                            <h3><?php the_title() ?></h3>
                                            <p><?php the_field('list_excerpt') ?></p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            
                    <?php 
                        endwhile;?>
                        <div class="col-12 pages">
                            <?php if($paged != 1): ?>
                            <div class="prev-page">                   
                                <?php previous_posts_link( sprintf( '<div><img src="'.get_template_directory_uri().'/dist/images/arrow-wiecej.svg"><span class="prev-next-title">%1$s</span></div>', __('poprzednie', 'text-domain')) ); ?>    
                            </div>
                            <?php endif; ?>
                            <div class="next-page">
                                <?php next_posts_link( sprintf( '<div><span class="prev-next-title">%1$s</span><img src="'.get_template_directory_uri().'/dist/images/arrow-wiecej.svg"></div>', __('więcej', 'text-domain')) , $project->max_num_pages ); ?>
                            </div>
                        </div>
                        <div class="col-12 go-back">
                            <a href="/">powrót <img src="<?php echo get_template_directory_uri() . '/dist/images/powrot.svg'?>"></a>
                        </div>
                        <?php wp_reset_postdata();
                    endif;
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
</main>


<?php get_footer(); ?>