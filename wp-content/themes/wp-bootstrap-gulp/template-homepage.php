<?php
/*
	Template name: Homepage
*/

get_header(); ?>

    <main class="front-page">
        <section class="header" class="parallax-window" data-parallax="scroll" data-image-src="<?php echo get_field( 'header' )['bg_image']['url'] ?>">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-md-10">
                        <h2 class="pre-title"><?php echo get_field( 'header' )['pre-title'] ?></h2>
                        <h1 id="header-heading">
                            <span class="blue"><?php echo get_field( 'header' )['clock'] ?></span>
                            <span class="orange"><?php echo get_field( 'header' )['sunny'] ?></span>
                        </h1>
                    </div>
                    <div class="col-12 col-md-2 serif-font">
						<?php echo get_field( 'header' )['motto'] ?>
                    </div>
                </div>
            </div>
			<?php if ( get_field( 'kidconnect_link' ) ): ?>
                <a href="<?php echo get_field( 'header' )['kidconnect_link'] ?>" class="kidconnect serif-font"><?php echo get_field( 'header' )['kidconnect_text'] ?></a>
			<?php endif; ?>
        </section>
        <?php if(get_field('place_after') === 'banner') get_template_part('partials/additional-section-2');?>
        <section class="calendar">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <table>
                            <tr class="serif-font">
                                <th><a href="https://calendar.google.com/calendar/embed?src=zegar.sloneczny%40gmail.com&ctz=Europe%2FWarsaw" target="_blank"><?php echo get_field( 'calendar' )['date_title'] ?></a></th>
                                <th><?php echo get_field( 'calendar' )['important_title'] ?></th>
                            </tr>
                            <tr>
                                <td class="date">
                                    <p>
										<?php echo date( "d" ) ?><br>
										<?php echo date( "m" ) ?>
                                    </p>
                                </td>
                                <td class="content">
									<?php echo get_field( 'calendar' )['important_content'] ?>
                                </td>
                            </tr>
                        </table>
                        <table class="mobile">
                            <tr>
                                <th>
                                    <a href="https://calendar.google.com/calendar/embed?src=zegar.sloneczny%40gmail.com&ctz=Europe%2FWarsaw" target="_blank">
										<?php echo get_field( 'calendar' )['date_title'] ?>
                                    </a>
                                </th>
                            </tr>
                            <tr>
                                <td class="date">
                                    <p>
										<?php echo date( "d" ) ?><br>
										<?php echo date( "m" ) ?>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table class="mobile">
                            <tr>
                                <th><?php echo get_field( 'calendar' )['important_title'] ?></th>
                            </tr>
                            <tr>
                                <td class="content">
									<?php echo get_field( 'calendar' )['important_content'] ?>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('place_after') === 'kalendarz') get_template_part('partials/additional-section-2');?>
        <section class="recruitment" id="rekrutacja">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php the_field( 'recruitment_title' ) ?></h2>
                        <ul class="nav nav-tabs" id="tab-recruitment" role="tablist">
							<?php if ( have_rows( 'recruitment_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'recruitment_tabs' ) ) : the_row(); ?>

                                    <li class="nav-item">
                                        <a class="nav-link<?php echo $i == 1 ? ' active' : '' ?>" id="tab-recruitment-<?php echo $i ?>" data-toggle="tab" href="#recruitment-<?php echo $i ?>" role="tab" aria-controls="recruitment" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                    </li>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-12 col-md-10 offset-md-1">
                        <div class="tab-content" id="recruitment-content">
							<?php if ( have_rows( 'recruitment_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'recruitment_tabs' ) ) : the_row(); ?>

                                    <div class="tab-pane with-read-more fade<?php echo $i == 1 ? ' show active' : '' ?>" id="recruitment-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-recruitment-<?php echo $i ?>">
										<?php the_sub_field( 'content' ); ?>
                                        <a class="read-more d-none" href="#"><?php the_field( 'recruitment_read_more' ) ?><?php echo file_get_contents( get_template_directory_uri() . '/dist/images/arrow-right.svg' ) ?></a>
                                    </div>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('place_after') === 'rekrutacja') get_template_part('partials/additional-section-2');?>
        <a href="<?php echo get_field( 'events' )['link'] ?>">
            <section class="img-section events" style="background-image: url('<?php echo get_field( 'events' )['background_image']['url'] ?>')">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><?php echo get_field( 'events' )['title'] ?></h2>
                        </div>
                    </div>
                </div>
            </section>
        </a>
        <?php if(get_field('place_after') === 'wydarzenia') get_template_part('partials/additional-section-2');?>
        <section class="program" id="program">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2 class="serif-font"><?php the_field( 'program_title' ) ?></h2>
                        <div class="tabs">
                            <ul class="nav nav-tabs" id="program-tabs" role="tablist">
								<?php if ( have_rows( 'program_tabs' ) ) : $i = 1; ?>
									<?php while ( have_rows( 'program_tabs' ) ) : the_row(); ?>

                                        <li class="nav-item">
                                            <a class="nav-link" id="tab-program-<?php echo $i ?>" data-toggle="tab" href="#program-<?php echo $i ?>" role="tab" aria-controls="program" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                        </li>

										<?php $i ++; endwhile; ?>
								<?php endif; ?>
                            </ul>

                            <div class="tab-content">
								<?php if ( have_rows( 'program_tabs' ) ) : $i = 1; ?>

									<?php while ( have_rows( 'program_tabs' ) ) : the_row(); ?>

                                        <div class="tab-pane" id="program-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-program-<?php echo $i ?>">
											<?php the_sub_field( 'content' ); ?>
                                        </div>

										<?php $i ++; endwhile; ?>

								<?php endif; ?>
                            </div>
                        </div>
                        <div class="program-images">
                            <div class="swiper-container program-swiper">
                                <div class="swiper-wrapper">
									<?php if ( get_field( 'program_activities' ) ) : ?>

										<?php foreach ( get_field( 'program_activities' ) as $activity ) : ?>
                                            <div class="swiper-slide">
												<?php if ( $activity['photo']['subtype'] === 'svg+xml' ):
													echo file_get_contents( $activity['photo']['url'] );
												else: ?>
                                                    <img src="<?php echo $activity['photo']['url'] ?>" alt="<?php echo $activity['photo']['title'] ?>">
												<?php endif; ?>
                                                <p><?php echo $activity['text']; ?></p>
                                            </div>

										<?php endforeach; ?>

									<?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="swiper-navigation">
                <div class="swiper-button-prev">
					<?php echo file_get_contents( get_template_directory() . '/dist/images/swiper-arrow-left.svg' ) ?>
                </div>
                <div class="swiper-button-next">
					<?php echo file_get_contents( get_template_directory() . '/dist/images/swiper-arrow-right.svg' ) ?>
                </div>
            </div>
        </section>
        <?php if(get_field('place_after') === 'program') get_template_part('partials/additional-section-2');?>
        <a class="section-link" href="<?php echo get_field( 'projects' )['link'] ?>">
            <section class="img-section projects">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><?php echo get_field( 'projects' )['title'] ?></h2>
                        </div>
                    </div>
                </div>
            </section>
        </a>
        <?php if(get_field('place_after') === 'projekty') get_template_part('partials/additional-section-2');?>
        <section class="about" id="o-nas">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php the_field( 'about_title' ) ?></h2>
                        <ul class="nav nav-tabs" id="tab-about" role="tablist">
							<?php if ( have_rows( 'about_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'about_tabs' ) ) : the_row(); ?>

                                    <li class="nav-item">
                                        <a class="nav-link<?php echo $i == 1 ? ' active' : '' ?>" id="tab-about-<?php echo $i ?>" data-toggle="tab" href="#about-<?php echo $i ?>" role="tab" aria-controls="about" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                    </li>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-12 col-md-10 offset-md-1">
                        <div class="tab-content" id="about-content">
							<?php if ( have_rows( 'about_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'about_tabs' ) ) : the_row(); ?>

                                    <div class="tab-pane fade<?php echo $i == 1 ? ' show active' : '' ?>" id="about-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-about-<?php echo $i ?>">
										<?php the_sub_field( 'content' ); ?>
                                    </div>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('place_after') === 'organizacja') get_template_part('partials/additional-section-2');?>
        <section class="parent-counsel" id="rada-rodzicow">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <h2><?php the_field( 'counsel_title' ) ?></h2>
                        <ul class="nav nav-tabs" id="tab-counsel" role="tablist">
							<?php if ( have_rows( 'counsel_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'counsel_tabs' ) ) : the_row(); ?>


                                    <li class="nav-item">
                                        <a class="nav-link<?php echo $i == 1 ? ' active' : '' ?>" id="tab-counsel-<?php echo $i ?>" data-toggle="tab" href="#counsel-<?php echo $i ?>" role="tab" aria-controls="counsel" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                    </li>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>
                        </ul>
                    </div>
                    <div class="col-12 col-md-10 offset-md-1">
                        <div class="tab-content" id="counsel-content">
							<?php if ( have_rows( 'counsel_tabs' ) ) : $i = 1; ?>

								<?php while ( have_rows( 'counsel_tabs' ) ) : the_row(); ?>

                                    <div class="tab-pane with-read-more fade<?php echo $i == 1 ? ' show active' : '' ?>" id="counsel-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-counsel-<?php echo $i ?>">
										<?php the_sub_field( 'content' ); ?>
                                        <a class="read-more d-none" href="#"><?php the_field( 'counsel_read_more' ) ?><?php echo file_get_contents( get_template_directory_uri() . '/dist/images/arrow-right.svg' ) ?></a>
                                    </div>

									<?php $i ++; endwhile; ?>

							<?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php if(get_field('place_after') === 'rada-rodzicow') get_template_part('partials/additional-section-2');?>
		<?php if ( get_field( 'enable_additional' ) ): ?>
            <section class="additional-section" id="dodatkowa-sekcja">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h2><?php the_field( 'additional_title' ) ?></h2>
                            <ul class="nav nav-tabs" id="tab-additional" role="tablist">
								<?php if ( have_rows( 'additional_tabs' ) ) : $i = 1; ?>

									<?php while ( have_rows( 'additional_tabs' ) ) : the_row(); ?>

                                        <li class="nav-item">
                                            <a class="nav-link<?php echo $i == 1 ? ' active' : '' ?>" id="tab-additional-<?php echo $i ?>" data-toggle="tab" href="#additional-<?php echo $i ?>" role="tab" aria-controls="additional" aria-selected="true"><?php the_sub_field( 'title' ); ?></a>
                                        </li>

										<?php $i ++; endwhile; ?>

								<?php endif; ?>
                            </ul>
                        </div>
                        <div class="col-12 col-md-10 offset-md-1">
                            <div class="tab-content" id="additional-content">
								<?php if ( have_rows( 'additional_tabs' ) ) : $i = 1; ?>

									<?php while ( have_rows( 'additional_tabs' ) ) : the_row(); ?>

                                        <div class="tab-pane fade<?php echo $i == 1 ? ' show active' : '' ?>" id="additional-<?php echo $i ?>" role="tabpanel" aria-labelledby="tab-additional-<?php echo $i ?>">
                                            <div class="content-wrapper">
                                                <div class="content">
													<?php the_sub_field( 'content' ); ?>
                                                </div>
                                            </div>
                                            <a class="read-more d-none" href="#"><?php the_field( 'additional_read_more' ) ?><?php echo file_get_contents( get_template_directory_uri() . '/dist/images/arrow-right.svg' ) ?></a>
                                        </div>

										<?php $i ++; endwhile; ?>

								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
		<?php endif; ?>
        <?php if(get_field('place_after') === 'dodatkowa-sekcja') get_template_part('partials/additional-section-2');?>
    </main>

<?php get_footer(); ?>